﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Apoteka.CustomRenderers;
using Apoteka.Droid;
using Java.Interop;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(MySearchBar), typeof(MySearchBarRenderer))]
namespace Apoteka.Droid
{
    class MySearchBarRenderer: SearchBarRenderer
    {
        public MySearchBarRenderer(Context context):base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            Control.Background = ContextCompat.GetDrawable(this.Context, Resource.Drawable.custom_search_view);
            
            var searchView = (Control as SearchView);
            var searchIconId = searchView.Resources.GetIdentifier("android:id/search_mag_icon", null, null);
            if (searchIconId > 0)
            {
                var searchPlateIcon = searchView.FindViewById(searchIconId);
                (searchPlateIcon as ImageView).SetColorFilter(Android.Graphics.Color.LightGray, PorterDuff.Mode.SrcIn);
            }

            var searchCancelIconId = searchView.Resources.GetIdentifier("android:id/search_close_btn", null, null);
            if (searchCancelIconId > 0)
            {
                var searchCancelIcon = searchView.FindViewById(searchCancelIconId);
                (searchCancelIcon as ImageView).SetColorFilter(Android.Graphics.Color.LightGray, PorterDuff.Mode.SrcIn);
            }

            var searchPlateIconId = searchView.Resources.GetIdentifier("android:id/search_plate", null, null);
            if (searchCancelIconId > 0)
            {
                var searchPlate = searchView.FindViewById(searchPlateIconId);
                searchPlate.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}