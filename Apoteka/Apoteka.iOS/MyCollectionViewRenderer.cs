﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using Apoteka.CustomRenderers;
using Apoteka.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MyCollectionView),typeof(MyCollectionViewRenderer))]
namespace Apoteka.iOS
{
    public class MyCollectionViewRenderer : CollectionViewRenderer
    {
        protected override void Dispose(bool disposing)
        {
            base.ItemsView.SelectedItem = null;
            base.ItemsView.ItemsSource = null;
            base.Dispose(disposing);
        }
    }
}