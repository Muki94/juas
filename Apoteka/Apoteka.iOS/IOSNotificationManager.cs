﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Apoteka.HelperModels;
using Apoteka.Interfaces;
using Foundation;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

[assembly:Dependency(typeof(Apoteka.iOS.IOSNotificationManager))]
namespace Apoteka.iOS
{
    public class IOSNotificationManager : INotificationManager
    {
        int messageId = -1;
        bool hasNotificationsPermission;
        public event EventHandler NotificationReceived;

        public void Initialize()
        {
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) =>
            {
                this.hasNotificationsPermission = approved;
            });
        }

        public void ReceiveNotification(string title, string message)
        {
            var args = new NotificationEventArgs()
            {
                Title = title,
                Message = message
            };

            NotificationReceived?.Invoke(null, args);
        }

        public int ScheduleNotification(string title, string message)
        {
            if (!hasNotificationsPermission)
                return -1;

            messageId++;

            var content = new UNMutableNotificationContent
            {
                Title = title,
                Body = message
            };

            var trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(0.25, false);

            var request = UNNotificationRequest.FromIdentifier(messageId.ToString(), content, trigger);
            UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) =>
            {
                if (err != null)
                {
                    throw new Exception($"Failed to schedule notification: {err}");
                }
            });

            return messageId;
        }
    }
}