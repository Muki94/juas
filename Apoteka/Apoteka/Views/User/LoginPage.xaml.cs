﻿using Apoteka.ViewModels;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.User
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginViewModel loginViewModel;
        public LoginPage()
        {
            InitializeComponent();
            FooterImage.Source = "Background1.png";
            BindingContext = loginViewModel = new LoginViewModel();
        }
        protected override void OnAppearing()
        {
            //Write the code of your page here
            base.OnAppearing();
        }

        private async void SignInButton_Pressed(object sender, System.EventArgs e)
        {
            try
            {
                Button signInButton = (Button)sender;

                loginViewModel.LoginUserCommand.Execute(null);

                signInButton.BackgroundColor = Color.FromHex("#157DC3");
                signInButton.TextColor = Color.White;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private void SignInButton_Released(object sender, System.EventArgs e)
        {
            Button signInButton = (Button)sender;

            signInButton.BackgroundColor = Color.White;
            signInButton.TextColor = Color.FromHex("#2186CA");
        }

        private async void Registration_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new RegistrationPage());
        }
    }
}