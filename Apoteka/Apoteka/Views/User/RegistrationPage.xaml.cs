﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.ViewModels;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.User
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationViewModel registrationViewModel;

        public RegistrationPage()
        {
            InitializeComponent();
            FooterImage.Source = "Background1.png";
            BindingContext = registrationViewModel = new RegistrationViewModel();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                registrationViewModel.RegisterUserCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}