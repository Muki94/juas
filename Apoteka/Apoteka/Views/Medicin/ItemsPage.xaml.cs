﻿using System.ComponentModel;
using Xamarin.Forms;

using Apoteka.ViewModels;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using System;
using Apoteka.BLL.DataTransferObjects;
using System.Linq;
using Apoteka.Helpers;
using System.Threading.Tasks;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;

namespace Apoteka.Views.Medicin
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ItemsViewModel();
        }

        protected async override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                if (viewModel.Items.Count == 0)
                    viewModel.LoadItemsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void SearchEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                await Task.Delay(2000);

                viewModel.SearchItemsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void ItemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var item = e.CurrentSelection.FirstOrDefault() as MedicinDTO;

                if (item == null)
                    return;

                await Navigation.PushAsync(new ItemDetailPage(item));

                // Manually deselect item.
                ((CollectionView)sender).SelectedItem = null;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}