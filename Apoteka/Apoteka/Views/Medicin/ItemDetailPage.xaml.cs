﻿using System.ComponentModel;
using Xamarin.Forms;

using Apoteka.ViewModels;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using Plugin.Geolocator;
using System;
using Apoteka.Helpers;
using Plugin.Permissions.Abstractions;
using Apoteka.BLL.DataTransferObjects;
using Apoteka.Views.Pharmacy;
using Apoteka.BLL.Services;
using System.Linq;
using Rg.Plugins.Popup.Services;
using Rg.Plugins.Popup.Pages;
using Apoteka.Views.Popups;

namespace Apoteka.Views.Medicin
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        public ItemDetailPage(MedicinDTO medicin)
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemDetailViewModel(medicin);
        }

        protected async override void OnAppearing()
        {
            try
            {
                viewModel.LoadItemCommand.Execute(viewModel.Item.Id);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void LocationsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.CurrentSelection.FirstOrDefault() is PharmacyMedicinDTO pharmacyMedicin)
                {
                    var pharmacies = await DependencyService.Get<MockPharmacyDataStore>().GetItemsAsync();

                    var pharmacy = pharmacies.Where(x => x.Id == Convert.ToInt32(pharmacyMedicin.PharmacyId)).FirstOrDefault(); 

                    await Navigation.PushAsync(new PharmacyDetailPage(pharmacy));
                }

                ((CollectionView)sender).SelectedItem = null;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void SubstituteItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.CurrentSelection.FirstOrDefault() is MedicinDTO medicin)
                    await Navigation.PushAsync(new ItemDetailPage(medicin));

                ((CollectionView)sender).SelectedItem = null;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}