﻿using Apoteka.BLL.Services;
using Apoteka.HelperModels;
using Apoteka.Helpers;
using Apoteka.Interfaces;
using Apoteka.Services;
using Apoteka.ViewModels;
using Apoteka.Views.Medicin;
using Apoteka.Views.Popups;
using Plugin.Connectivity;
using Plugin.Messaging;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Apoteka.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private MarketingItemViewModel marketingViewModel;
        private const int marketingIntervalDuration = 7000;

        private NotificationViewModel notificationViewModel;
        private const int notificationCheckIntervalDuration = 300000;

        Timer ChangeSlideTimer = null;
        Timer CheckUserNotifications = null;

        enum Navigations
        {
            Medicin,
            Pharmacy,
            Map,
            Contact,
            Login,
            Logout,
            WebShop,
            CallUs
        }

        public MainPage()
        {
            InitializeComponent();

            BindingContext = marketingViewModel = new MarketingItemViewModel();
            notificationViewModel = new NotificationViewModel();

            MedicinImage.Source = "medicine.png";
            PharmacyImage.Source = "pharmacy.png";
            MapImage.Source = "redcross.png";
            ContactImage.Source = "phone.png";
            WebShopImage.Source = "shopingCartFull.png";
            LoginImage.Source = "enter.png";
            LogoutImage.Source = "enter.png";
            CallUsImage.Source = "whatsapp.png";

            //logic for automatic slider
            ChangeSlideTimer = new Timer();
            ChangeSlideTimer.Elapsed += new ElapsedEventHandler(ChangeCarouselSlide);
            ChangeSlideTimer.Interval = marketingIntervalDuration;
            ChangeSlideTimer.Enabled = true;

            CheckUserNotifications = new Timer();
            CheckUserNotifications.Elapsed += new ElapsedEventHandler(LoadUserNotifications);
            CheckUserNotifications.Interval = notificationCheckIntervalDuration;
            CheckUserNotifications.Enabled = true;
        }

        private async void ChangeCarouselSlide(object sender, System.EventArgs e)
        {
            try
            {
                await new TaskFactory().StartNew( async () =>
                {
                    await Device.InvokeOnMainThreadAsync(() =>
                    {
                        if (MarketingItemsSlider.Position >= (marketingViewModel.MarketingItems.Count - 1))
                            MarketingItemsSlider.Position = 0;
                        else
                            MarketingItemsSlider.Position += 1;
                    });
                });
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void NavigationButton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                ChangeSlideTimer.Stop();

                int navigationButton = Convert.ToInt32(((TappedEventArgs)e).Parameter);

                SpinnerOn(true);

                switch ((Navigations)navigationButton)
                {
                    case Navigations.Medicin:
                        await Navigation.PushAsync(new Medicin.ItemsPage());
                        break;
                    case Navigations.Pharmacy:
                        await Navigation.PushAsync(new Pharmacy.PharmacyPage());
                        break;
                    case Navigations.Map:
                        await Navigation.PushAsync(new Map.MapsPage());
                        break;
                    case Navigations.Contact:
                        await Navigation.PushAsync(new AboutPage());
                        break;
                    case Navigations.Login:
                        await Navigation.PushAsync(new User.LoginPage());
                        break;
                    case Navigations.Logout:
                        LoginViewModel.LoggedId = null;
                        LoginViewModel.LoggedUserName = null;
                        await Navigation.PushAsync(new User.LoginPage());
                        break;
                    case Navigations.WebShop:
                        await Launcher.TryOpenAsync("https://shop.apoteke-sarajevo.ba/");
                        break;
                    case Navigations.CallUs:
                        if (Device.RuntimePlatform == Device.iOS)
                             PhoneDialer.Open("+38733223399");
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            var phoneDialer = CrossMessaging.Current.PhoneDialer;
                            if (phoneDialer.CanMakePhoneCall)
                                phoneDialer.MakePhoneCall("+38733223399");
                        }
                        break;
                    default:
                        break;
                }

                SpinnerOn(false);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        protected async override void OnAppearing()
        {
            try
            {
                if (String.IsNullOrEmpty(LoginViewModel.LoggedUserName) &&
                    String.IsNullOrEmpty(LoginViewModel.LoggedId))
                {
                    LoginSL.IsVisible = true;
                    LogoutSL.IsVisible = false;
                }
                else
                {
                    LoginSL.IsVisible = false;
                    LogoutSL.IsVisible = true;
                }

                marketingViewModel.LoadMarketingItemsCommand.Execute(null);
                ChangeSlideTimer.Start();
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private void SpinnerOn(bool isOn)
        {
            Spinner.IsRunning = isOn;
        }

        private async void LoadUserNotifications(object sender, EventArgs e)
        {
            try
            {
                notificationViewModel.ShowUserNotificationsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}