﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.ViewModels;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.Pharmacy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PharmacyItemDetailPage : ContentPage
    {
        public PharmacyItemDetailViewModel viewModel;

        public PharmacyItemDetailPage(PharmacyItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        private async void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            try
            {
                var checkbox = (CheckBox)sender;

                if (viewModel.Medicin != null && checkbox.IsChecked != viewModel.Medicin.OldFollowedValue)
                {
                    viewModel.FollowItemCommand.Execute(new NotificationDTO
                    {
                        UserId = LoginViewModel.LoggedId,
                        PharmacyId = viewModel.Pharmacy.Id.ToString(),
                        ProductId = viewModel.Medicin.Id.ToString(),
                        Follow = checkbox.IsChecked
                    });
                }
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}