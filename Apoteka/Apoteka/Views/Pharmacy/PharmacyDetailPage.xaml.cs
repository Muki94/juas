﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.Helpers;
using Apoteka.ViewModels;
using Apoteka.Views.Map;
using Apoteka.Views.Medicin;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.Pharmacy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PharmacyDetailPage : ContentPage
    {
        PharmacyDetailViewModel viewModel;

        public PharmacyDetailPage(PharmacyDTO item)
        {
            InitializeComponent();
            BindingContext = this.viewModel = new PharmacyDetailViewModel(item);
        }

        protected async override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                await viewModel.SetMapWithLocations();

                if (viewModel.Pharmacy != null && viewModel.Locations != null && viewModel.Locations.Count > 0)
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(viewModel.Locations[0].Latitude, viewModel.Locations[0].Longitude), Distance.FromMiles(1)));
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        public async void SearchEntry_TextChanged(object sender, TextChangedEventArgs eventArgs)
        {
            try
            {
                await Task.Delay(2000);

                viewModel.SearchItemsForStoreByParamCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void PharmacyItemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var item = e.CurrentSelection.FirstOrDefault() as MedicinDTO;

                if (item == null)
                    return;

                await Navigation.PushAsync(new PharmacyItemDetailPage(new PharmacyItemDetailViewModel(viewModel.Pharmacy, item)));

                // Manually deselect item.
                ((CollectionView)sender).SelectedItem = null;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void FollowItem_Toggled(object sender, ToggledEventArgs e)
        {
            try
            {
                var switchControl = (Switch)sender;
                var item = (MedicinDTO)switchControl.BindingContext;

                if (item != null && switchControl.IsToggled != item.OldFollowedValue)
                {
                    viewModel.FollowItemCommand.Execute(new NotificationDTO
                    {
                        UserId = LoginViewModel.LoggedId,
                        PharmacyId = viewModel.Pharmacy.Id.ToString(),
                        ProductId = item.Id.ToString(),
                        Follow = switchControl.IsToggled
                    });
                }
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}