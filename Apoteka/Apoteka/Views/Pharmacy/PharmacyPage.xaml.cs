﻿
using Apoteka.BLL.DataTransferObjects;
using Apoteka.ViewModels;
using Apoteka.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.Pharmacy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [DesignTimeVisible(false)]
    public partial class PharmacyPage : ContentPage
    {
        PharmaciesViewModel viewModel;

        public PharmacyPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new PharmaciesViewModel();
        }

        protected async override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                if (viewModel.Pharmacies.Count == 0)
                    viewModel.LoadPharmaciesCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void SearchEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                await Task.Delay(2000);
                
                viewModel.SearchPharmaciesCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }

        private async void PharmaciesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var item = e.CurrentSelection.FirstOrDefault() as PharmacyDTO;

                if (item == null)
                    return;

                await Navigation.PushAsync(new PharmacyDetailPage(item));

                // Manually deselect item.
                ((CollectionView)sender).SelectedItem = null;
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}