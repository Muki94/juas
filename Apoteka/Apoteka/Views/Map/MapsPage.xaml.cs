﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Apoteka.ViewModels;
using System;
using Apoteka.BLL.DataTransferObjects;
using Xamarin.Forms.Maps;
using Rg.Plugins.Popup.Services;
using Apoteka.Views.Popups;

namespace Apoteka.Views.Map
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapsPage : ContentPage
    {
        MapsViewModel mapsViewModel;

        public MapsPage()
        {
            InitializeComponent();
            BindingContext = mapsViewModel = new MapsViewModel();
        }

        protected async override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                mapsViewModel.LoadLocationsCommand.Execute(null);
            }
            catch (Exception ex)
            {
                await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", ex.Message, "OK")));
            }
        }
    }
}