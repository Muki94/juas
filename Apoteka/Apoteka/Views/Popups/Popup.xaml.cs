﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.ViewModels;
using Rg.Plugins.Popup;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apoteka.Views.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Popup : PopupPage
    {
        public Popup(PopupViewModel popup)
        {
            InitializeComponent();
            BindingContext = popup;
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync(true);
        }
    }
}