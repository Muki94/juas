﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.Helpers
{
    public class GlobalFilterAttributes
    {
        public int Id { get; set; }
        public string SearchParameter { get; set; }
        public string ItemIndex { get; set; }
    }
}
