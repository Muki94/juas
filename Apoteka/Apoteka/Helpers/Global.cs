﻿using Apoteka.BLL.DataTransferObjects;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.Helpers
{
    public static class Global
    {
        public static PermissionStatus LocationPermissionStatus { get; set; }

        public static async Task<LocationDTO> GetLocationByAddressAsync(string address)
        {
            try
            {
                var location = (await Xamarin.Essentials.Geocoding.GetLocationsAsync(address)).FirstOrDefault();
                return new LocationDTO()
                {
                    Longitude = location.Longitude,
                    Latitude = location.Latitude,
                    Address = address,
                    Id = 0,
                    LocationName = address
                };
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
