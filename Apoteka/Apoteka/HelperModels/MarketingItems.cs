﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Apoteka.HelperModels
{
    public class MarketingItems
    {
        public int Id { get; set; }
        public ImageSource Image { get; set; }
    }
}
