﻿using Apoteka.BLL.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace Apoteka.HelperModels
{
    public class Locations : LocationDTO
    {
        public Position Position { get; set; }
    }
}
