﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.Interfaces
{
    public interface INotificationService
    {
        void ShowNotification(string title, string message);
    }
}
