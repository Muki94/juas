﻿using Apoteka.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Apoteka.Services
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationManager notificationManager;

        public NotificationService()
        {
            notificationManager = DependencyService.Get<INotificationManager>();
        }

        public void ShowNotification(string title, string message)
        {
            notificationManager.ScheduleNotification(title, message);
        }
    }
}
