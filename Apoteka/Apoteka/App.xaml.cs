﻿using System;
using Xamarin.Forms;
using Apoteka.Views;
using Microsoft.AppCenter.Push;
using Microsoft.AppCenter;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Xamarin.Essentials;
using System.Timers;
using Apoteka.BLL.Services;
using Plugin.Geolocator;
using Apoteka.Helpers;
using Apoteka.Interfaces;
using Apoteka.Services;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Services;
using Apoteka.Views.Popups;

namespace Apoteka
{
    public partial class App : Application
    {
        public App()
        {
            Xamarin.Forms.Device.SetFlags(new string[] { "IndicatorView_Experimental" });
            InitializeComponent();

            DependencyService.Register<MockItemDataStore>();
            DependencyService.Register<MockLocationDataStore>();
            DependencyService.Register<MockMarketingItemDataStore>();
            DependencyService.Register<MockPharmacyDataStore>();
            DependencyService.Register<MockPharmacyMedicinDataStore>();
            DependencyService.Register<MockNotificationDataStore>();
            DependencyService.Register<MockUserDataStore>();
            DependencyService.Register<INotificationService, NotificationService>();
            DependencyService.Register<IPopupNavigation>();

            MainPage = new NavigationPage(new MainPage());
        }

        protected async override void OnStart()
        {
            await GetPermissions();
            CheckConnection();
            CheckGPSConnection();

            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged; ;
        }

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            CheckGPSConnection();
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            CheckConnection();
        }

        private void CheckConnection()
        {
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current != NetworkAccess.Internet)
                {
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new ViewModels.PopupViewModel("Info","Provjerite svoju internet konekciju kako bi nastavili dalje!", "OK")));
                    });
                }
            }
            catch (Exception ex)
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                {
                    await PopupNavigation.Instance.PushAsync(new Popup(new ViewModels.PopupViewModel("Info", $"{ex.Message} {ex.InnerException?.Message}", "OK")));
                });
            }
        }

        private void CheckGPSConnection()
        {
            try
            {
                if (!CrossGeolocator.IsSupported || !CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new ViewModels.PopupViewModel("Info", "Potrebno je ukljuciti GPS kako bi koristili aplikaciju!", "OK")));
                    });
                }
            }
            catch (Exception ex)
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                {
                    await PopupNavigation.Instance.PushAsync(new Popup(new ViewModels.PopupViewModel("Error", $"{ex.Message} {ex.InnerException?.Message}", "OK")));
                });
            }
        }

        public async Task GetPermissions()
        {
            try
            {
                Global.LocationPermissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.LocationWhenInUse);

                if (Global.LocationPermissionStatus != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.LocationWhenInUse))
                    {
                        Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                        {
                            await PopupNavigation.Instance.PushAsync(new Popup(new ViewModels.PopupViewModel("Info", "Potrebno je da dozvolite permisije za lokaciju kako bi mogli imati potpuni doživljaj!", "OK")));
                        });
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.LocationWhenInUse);

                    if (results.ContainsKey(Permission.LocationWhenInUse))
                        Global.LocationPermissionStatus = results[Permission.LocationWhenInUse];
                }
            }
            catch (Exception ex)
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    Application.Current.MainPage.DisplayAlert("Error", $"{ex.Message}, {ex.InnerException?.Message}", "OK");
                });
            }
        }
    }
}
