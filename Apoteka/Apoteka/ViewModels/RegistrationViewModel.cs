﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.DAL.ApiServices;
using Apoteka.Helpers;
using Apoteka.Views.Popups;
using MvvmHelpers.Commands;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class RegistrationViewModel : BaseViewModel
    {
        #region firstName
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set
            {
                SetProperty<string>(ref firstName, value);
                IsFirstnameValid = !String.IsNullOrEmpty(firstName);
            }
        }

        private bool isFirstnameValid = false;
        public bool IsFirstnameValid
        {
            get { return isFirstnameValid; }
            set
            {
                SetProperty<bool>(ref isFirstnameValid, value);
            }
        }
        #endregion

        #region lastName
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                SetProperty<string>(ref lastName, value);
                IsLastnameValid = !String.IsNullOrEmpty(lastName);
            }
        }

        private bool isLastnameValid = false;
        public bool IsLastnameValid
        {
            get { return isLastnameValid; }
            set
            {
                SetProperty<bool>(ref isLastnameValid, value);
            }
        }
        #endregion

        #region username
        private string username;
        public string Username
        {
            get { return username; }
            set
            {
                SetProperty<string>(ref username, value);
                IsUsernameValid = !String.IsNullOrEmpty(username);
            }
        }

        private bool isUsernameValid = false;
        public bool IsUsernameValid
        {
            get { return isUsernameValid; }
            set { SetProperty<bool>(ref isUsernameValid, value); }
        }
        #endregion

        #region email
        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                SetProperty<string>(ref email, value);

                if (String.IsNullOrEmpty(email))
                {
                    EmailErrorText = "Polje email je obavezno";
                    IsEmailValid = false;
                    return;
                }

                string pattern = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
                var regex = new Regex(pattern, RegexOptions.IgnoreCase);
                var regexValid = regex.IsMatch(email);

                if (!regexValid)
                {
                    EmailErrorText = "Neispravan email format";
                    IsEmailValid = false;
                    return;
                }

                IsEmailValid = true;
            }
        }

        private bool isEmailValid = false;
        public bool IsEmailValid
        {
            get
            {
                return isEmailValid;
            }

            set
            {
                SetProperty<bool>(ref isEmailValid, value);
            }
        }

        private string emailErrorText = "Polje email je obavezno";
        public string EmailErrorText
        {
            get { return emailErrorText; }
            set { SetProperty<string>(ref emailErrorText, value); }
        }
        #endregion

        public bool AreElementsValid
        {
            get
            {
                return IsFirstnameValid &&
                       IsLastnameValid &&
                       IsUsernameValid &&
                       IsEmailValid;
            }
        }

        private string registerButtonText = "REGISTRUJ SE";
        public string RegisterButtonText
        {
            get { return registerButtonText; }
            set { SetProperty(ref registerButtonText, value); }
        }

        public ICommand RegisterUserCommand { get; set; }

        public RegistrationViewModel()
        {
            Title = "Registracija";
            RegisterUserCommand = new AsyncCommand(ExecuteRegisterUserCommand);
        }

        async Task ExecuteRegisterUserCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            RegisterButtonText = "";

            try
            {
                if (!AreElementsValid)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", "Niste popunili sva polja za registraciju!", "OK")));
                    });

                    return;
                }

                var user = await DependencyService.Get<MockUserDataStore>().AddItemAsync(new UserDTO
                {
                    FirstName = FirstName,
                    LastName = LastName,
                    Username = Username,
                    Email = Email
                });

                if (user != null)
                {
                    if (user.Code != null && user.Message != null)
                    {
                        string message = user.Message.Split('<')[0];

                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Error", message, "OK")));
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Success", @"Vaš korisnički nalog je uspješno kreiran za JUAS servise. 
                                                                                                               Mail sa lozinkom za JUAS servise je poslan (provjerite junk folder). 
                                                                                                               Ovoj aplikaciji pristupate samo sa username i email adresom koju ste unijeli.", "OK")));

                            await Application.Current.MainPage.Navigation.PopAsync();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
                RegisterButtonText = "REGISTRUJ SE";
            }
        }
    }
}
