﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.DAL.Models;
using Apoteka.HelperModels;
using Apoteka.Helpers;
using Apoteka.Views.Popups;
using MvvmHelpers;
using MvvmHelpers.Commands;
using Plugin.Geolocator;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Apoteka.ViewModels
{
    public class PharmacyDetailViewModel : BaseViewModel
    {
        public ObservableRangeCollection<MedicinDTO> Medicins { get; set; }
        public ObservableRangeCollection<Locations> Locations { get; set; }

        private PharmacyDTO pharmacy = null;
        public PharmacyDTO Pharmacy
        {
            get
            {
                return pharmacy;
            }
            set
            {
                SetProperty(ref pharmacy, value);
            }
        }

        private GlobalFilterAttributes filter;
        public GlobalFilterAttributes Filter
        {
            get { return filter; }
            set { SetProperty(ref filter, value); }
        }

        public ICommand SearchItemsForStoreByParamCommand { get; set; }
        public ICommand FollowItemCommand { get; set; }

        public PharmacyDetailViewModel(PharmacyDTO item)
        {
            Filter = new GlobalFilterAttributes();
            Medicins = new ObservableRangeCollection<MedicinDTO>();
            Locations = new ObservableRangeCollection<Locations>();

            SearchItemsForStoreByParamCommand = new AsyncCommand(ExecuteSearchItemsForStoreByParam);
            FollowItemCommand = new AsyncCommand<NotificationDTO>(ExecuteFollowItemCommand);
            Pharmacy = item;
        }

        async Task ExecuteSearchItemsForStoreByParam()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                List<MedicinDTO> items = new List<MedicinDTO>();

                if (Pharmacy != null && Pharmacy.Id != default(int) && !String.IsNullOrEmpty(Filter?.SearchParameter))
                    items = await DependencyService.Get<MockPharmacyMedicinDataStore>().GetItemsByPharmacyAsync(Pharmacy.Id, "0", Filter.SearchParameter);

                Medicins.Clear();

                if (items == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Detalji artikla nisu mogli biti učitani. 
                                                                                                        Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                List<NotificationDTO> followedItems = new List<NotificationDTO>();

                if (!String.IsNullOrEmpty(LoginViewModel.LoggedId))
                    followedItems = await DependencyService.Get<MockNotificationDataStore>().GetActiveItemsByUserIdAsync(LoginViewModel.LoggedId);

                for (var i = 0; i < items.Count; i++)
                {
                    int itemAvailabilityInShop = Convert.ToInt32(items[i].Availibility);

                    items[i].IsAvailableText = itemAvailabilityInShop > 0 ? "DOSTUPNO" : "NEDOSTUPNO";
                    items[i].IsAvailableColor = itemAvailabilityInShop > 0 ? "#05B90D" : "#DF240D";
                    items[i].CanFollowItem = itemAvailabilityInShop == 0 && !String.IsNullOrEmpty(LoginViewModel.LoggedId) ? true : false;
                    items[i].IsFollowed = followedItems?.Where(x => x.PharmacyId == Pharmacy.Id.ToString() &&
                                                                x.ProductId == items[i].Id.ToString())
                                                   ?.FirstOrDefault() != null ? true
                                                                              : false;
                    items[i].OldFollowedValue = items[i].IsFollowed;

                }

                Medicins.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteFollowItemCommand(NotificationDTO notification)
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                if (notification.Follow)
                    await DependencyService.Get<MockNotificationDataStore>().AddItemAsync(notification);
                else
                    await DependencyService.Get<MockNotificationDataStore>().DeleteNotificationAsync(notification);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }

        //TODO refactor (napravi da je globalna funkcija jer se koristi na vise mjesta)
        public async Task SetMapWithLocations()
        {
            try
            {
                if (Locations != null && Locations.Count == 0)
                {
                    var addressGeolocation = await Global.GetLocationByAddressAsync(Pharmacy.AddressInfo);

                    if (addressGeolocation != null)
                        Locations.Add(new Locations
                        {
                            Id = addressGeolocation.Id,
                            Address = addressGeolocation.Address,
                            Latitude = addressGeolocation.Latitude,
                            Longitude = addressGeolocation.Longitude,
                            LocationName = addressGeolocation.LocationName,
                            Position = new Position(addressGeolocation.Latitude, addressGeolocation.Longitude)
                        });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
