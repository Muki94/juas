﻿using Apoteka.BLL.DataTransferObjects;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public ObservableCollection<ContactDTO> Contacts { get; set; }

        public AboutViewModel()
        {
            Contacts = new ObservableCollection<ContactDTO>()
            {
                new ContactDTO
                {
                    DepartmentName = "KABINET GENERALNOG DIREKTORA",
                    DepartmentPhoneNumber = "+ 387 33 722 660",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "info@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA PRODAJU",
                    DepartmentPhoneNumber = "+ 387 33 722 672",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "prodaja@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA NABAVKU",
                    DepartmentPhoneNumber = "+ 387 33 476 127",
                    DepartmentFax = "+ 387 33 476 122",
                    DepartmentEmail = "nabavna@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA PRAVNE POSLOVE",
                    DepartmentPhoneNumber = "+ 387 33 722 673",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "pravna@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA INFORMACIONE TEHNOLOGIJE",
                    DepartmentPhoneNumber = "+ 387 33 721 623",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "it@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA FINANCIJE I RAČUNOVODSTVO",
                    DepartmentPhoneNumber = "+ 387 33 722 660",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "finansije@apoteke-sarajevo.ba"
                },
                new ContactDTO
                {
                    DepartmentName = "SLUŽBA ZA ISTRAŽIVANJE I RAZVOJ",
                    DepartmentPhoneNumber = "+ 387 33 725 140",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = ""
                },
                new ContactDTO
                {
                    DepartmentName = "CALL CENTAR",
                    DepartmentPhoneNumber = "+ 387 33 22 33 99",
                    DepartmentFax = "+ 387 33 722 667",
                    DepartmentEmail = "callcentar@apoteke-sarajevo.ba"
                }
            };
        }
    }
}