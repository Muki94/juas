﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;
using Apoteka.Helpers;
using MvvmHelpers.Commands;
using MvvmHelpers;
using Rg.Plugins.Popup.Services;
using Apoteka.Views.Popups;

namespace Apoteka.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableRangeCollection<MedicinDTO> Items { get; set; }

        private int itemTreshold = 1;
        public int ItemTreshold
        {
            get
            {
                return itemTreshold;
            }

            set
            {
                SetProperty<int>(ref itemTreshold, value);
            }
        }

        private GlobalFilterAttributes filter;
        public GlobalFilterAttributes Filter
        {
            get { return filter; }
            set { SetProperty<GlobalFilterAttributes>(ref filter, value); }
        }

        public ICommand LoadItemsCommand { get; set; }
        public ICommand ItemTresholdReachedCommand { get; set; }
        public ICommand SearchItemsCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Lijekovi";
            itemTreshold = 1;
            Filter = new GlobalFilterAttributes();
            Items = new ObservableRangeCollection<MedicinDTO>();
            LoadItemsCommand = new AsyncCommand(ExecuteLoadItemsCommand);
            ItemTresholdReachedCommand = new AsyncCommand(ExecuteItemTresholdReachedCommand);
            SearchItemsCommand = new AsyncCommand(ExecuteSearchItemsCommand);
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();

                List<MedicinDTO> items = new List<MedicinDTO>();

                if (String.IsNullOrEmpty(Filter?.SearchParameter))
                    items = await DependencyService.Get<MockItemDataStore>().GetItemsAsync("0", true);
                else
                    items = await DependencyService.Get<MockItemDataStore>().GetItemsByParamAsync("0", Filter.SearchParameter);

                if (items == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Artikli nisu mogli biti učitani. 
                                                                             Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                Items.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteItemTresholdReachedCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                List<MedicinDTO> items = null;

                if (String.IsNullOrEmpty(Filter.SearchParameter))
                    items = await DependencyService.Get<MockItemDataStore>().GetItemsAsync(Items.Count.ToString(), true);
                else
                    items = await DependencyService.Get<MockItemDataStore>().GetItemsByParamAsync(Items.Count.ToString(), Filter.SearchParameter);

                if (items == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Artikli nisu mogli biti učitani. 
                                                                             Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                if (items.Count == 0)
                    ItemTreshold = -1;

                Items.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteSearchItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();

                var items = await DependencyService.Get<MockItemDataStore>().GetItemsByParamAsync("0", Filter.SearchParameter);

                if (items == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Artikli nisu mogli biti učitani. 
                                                                             Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                Items.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}