﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.HelperModels;
using Apoteka.Views;
using Apoteka.Views.Popups;
using MvvmHelpers;
using MvvmHelpers.Commands;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class MarketingItemViewModel : BaseViewModel
    {
        public ObservableRangeCollection<MarketingItems> MarketingItems { get; set; }

        private int firstItemIndex = 0;
        public int FirstItemIndex
        {
            get { return firstItemIndex; }
            set { SetProperty<int>(ref firstItemIndex, value); }
        }

        public ICommand LoadMarketingItemsCommand { get; set; }

        public MarketingItemViewModel()
        {
            MarketingItems = new ObservableRangeCollection<MarketingItems>();
            LoadMarketingItemsCommand = new AsyncCommand(ExecuteLoadMarketingItemsCommand);
        }

        private async Task ExecuteLoadMarketingItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                MarketingItems.Clear();

                var marcetingItems = await DependencyService.Get<MockMarketingItemDataStore>().GetItemsAsync(true);

                if (marcetingItems == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Marketing artikli nisu mogli biti učitani. 
                                                                                                        Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                marcetingItems?.ForEach((x) =>
                {
                    MarketingItems.Add(new MarketingItems
                    {
                        Id = x.Id,
                        Image = ImageSource.FromStream(() =>
                        {
                            var ms = new MemoryStream(x.Image);
                            ms.Position = 0;
                            return ms;
                        })
                    });
                });

                FirstItemIndex = MarketingItems.FirstOrDefault().Id;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
