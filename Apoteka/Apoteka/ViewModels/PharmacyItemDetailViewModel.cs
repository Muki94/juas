﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class PharmacyItemDetailViewModel : BaseViewModel
    {
        private MedicinDTO medicin;
        public MedicinDTO Medicin
        {
            get { return medicin; }
            set {
                SetProperty<MedicinDTO>(ref medicin, value);
            }
        }

        private PharmacyDTO pharmacy;
        public PharmacyDTO Pharmacy
        {
            get { return pharmacy; }
            set
            {
                SetProperty<PharmacyDTO>(ref pharmacy, value);
            }
        }

        public ICommand FollowItemCommand;

        public PharmacyItemDetailViewModel(PharmacyDTO pharmacy, MedicinDTO medicin)
        {
            Medicin = medicin;
            Pharmacy = pharmacy;

            FollowItemCommand = new AsyncCommand<NotificationDTO>(ExecuteFollowItemCommand);
        }

        async Task ExecuteFollowItemCommand(NotificationDTO notification)
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                if (notification.Follow)
                    await DependencyService.Get<MockNotificationDataStore>().AddItemAsync(notification);
                else
                    await DependencyService.Get<MockNotificationDataStore>().DeleteNotificationAsync(notification);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
