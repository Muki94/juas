﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.Helpers;
using Apoteka.Views.Popups;
using MvvmHelpers;
using MvvmHelpers.Commands;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class PharmaciesViewModel : BaseViewModel
    {
        public ObservableRangeCollection<PharmacyDTO> Pharmacies { get; set; }

        private GlobalFilterAttributes filter;
        public GlobalFilterAttributes Filter
        {
            get { return filter; }
            set { SetProperty(ref filter, value); }
        }

        public ICommand LoadPharmaciesCommand { get; set; }
        public ICommand SearchPharmaciesCommand { get; set; }

        public PharmaciesViewModel()
        {
            Title = "Apoteke";
            Pharmacies = new ObservableRangeCollection<PharmacyDTO>();
            Filter = new GlobalFilterAttributes();
            LoadPharmaciesCommand = new AsyncCommand(ExecuteLoadPharmaciesCommand);
            SearchPharmaciesCommand = new AsyncCommand(ExecuteSearchPharmaciesCommand);
        }

        async Task ExecuteLoadPharmaciesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Pharmacies.Clear();

                var pharmacies = await DependencyService.Get<MockPharmacyDataStore>().GetItemsAsync(true);

                if (pharmacies == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Apoteke nisu mogle biti učitane. 
                                                                                                        Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                Pharmacies.AddRange(pharmacies);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteSearchPharmaciesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Pharmacies.Clear();

                var pharmacies = await DependencyService.Get<MockPharmacyDataStore>().GetItemsAsync(true);

                if (pharmacies == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", @"Apoteke nisu mogle biti učitane. 
                                                                                                        Molimo Vas da provjerite svoju internet konekciju!", "OK")));
                    });

                    return;
                }

                var items = !String.IsNullOrEmpty(Filter.SearchParameter)
                            ? pharmacies.Where(x => x.Address.ToUpper().Contains(Filter.SearchParameter.ToUpper()) ||
                                                    x.City.ToUpper().Contains(Filter.SearchParameter.ToUpper()) ||
                                                    x.Text.ToUpper().Contains(Filter.SearchParameter.ToUpper()))
                                                .ToList()
                            : pharmacies;

                Pharmacies.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
