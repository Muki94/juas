﻿
using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Apoteka.HelperModels;
using MvvmHelpers;
using MvvmHelpers.Commands;
using Rg.Plugins.Popup.Services;
using Apoteka.Views.Popups;
using System.Linq;

namespace Apoteka.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        //collections
        public ObservableRangeCollection<PharmacyMedicinDTO> ItemLocations { get; set; }
        public ObservableRangeCollection<Locations> Locations { get; set; }
        public ObservableRangeCollection<MedicinDTO> SubstituteItems { get; set; }

        private int itemTreshold = 1;
        public int ItemTreshold
        {
            get
            {
                return itemTreshold;
            }

            set
            {
                SetProperty<int>(ref itemTreshold, value);
            }
        }

        private MedicinDTO item;
        public MedicinDTO Item
        {
            get { return item; }
            set
            {
                SetProperty<MedicinDTO>(ref item, value);
            }
        }

        private bool locationsVisible;
        public bool LocationsVisibel
        {
            get { return locationsVisible; }
            set
            {
                SetProperty<bool>(ref locationsVisible, value);
            }
        }

        //commands
        public ICommand LoadItemCommand { get; set; }
        public ICommand LocationsItemTresholdReachedCommand { get; set; }
        public ICommand SubstituteItemsTresholdReachedCommand { get; set; }

        public ItemDetailViewModel()
        {
            LocationsVisibel = true;
            Item = new MedicinDTO();
            Locations = new ObservableRangeCollection<Locations>();
            ItemLocations = new ObservableRangeCollection<PharmacyMedicinDTO>();
            SubstituteItems = new ObservableRangeCollection<MedicinDTO>();
            LoadItemCommand = new AsyncCommand(ExecuteLoadItemCommand);
            LocationsItemTresholdReachedCommand = new AsyncCommand(ExecuteLocationsItemTresholdReachedCommand);
            SubstituteItemsTresholdReachedCommand = new AsyncCommand(ExecuteSubstituteItemsTresholdReachedCommand);
        }

        public ItemDetailViewModel(MedicinDTO medicin)
        {
            LocationsVisibel = true;
            Item = medicin;
            Locations = new ObservableRangeCollection<Locations>();
            ItemLocations = new ObservableRangeCollection<PharmacyMedicinDTO>();
            SubstituteItems = new ObservableRangeCollection<MedicinDTO>();
            LoadItemCommand = new AsyncCommand(ExecuteLoadItemCommand);
            LocationsItemTresholdReachedCommand = new AsyncCommand(ExecuteLocationsItemTresholdReachedCommand);
            SubstituteItemsTresholdReachedCommand = new AsyncCommand(ExecuteSubstituteItemsTresholdReachedCommand);
        }

        async Task ExecuteLoadItemCommand()
        {
            try
            {
                IsBusy = true;
                ItemLocations.Clear();

                List<PharmacyMedicinDTO> items = new List<PharmacyMedicinDTO>();

                items = await DependencyService.Get<MockItemDataStore>().GetItemLocationsAsync(Item.Id, ItemLocations.Count);

                if (items == null || items.Count == 0)
                {
                    LocationsVisibel = false;
                    List<MedicinDTO> substitueItems = await DependencyService.Get<MockItemDataStore>().GetItemsByParamAsync("0", Item.Atc);
                    SubstituteItems.AddRange(substitueItems.Where(x => x.Id != Item.Id).ToList());
                }
                else
                {
                    ItemLocations.AddRange(items);

                    await SetMapWithLocations();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteLocationsItemTresholdReachedCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                List<PharmacyMedicinDTO> items = new List<PharmacyMedicinDTO>();

                items = await DependencyService.Get<MockItemDataStore>().GetItemLocationsAsync(Item.Id, ItemLocations.Count);

                if (items == null)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup( new PopupViewModel("Info", @"Dostupne lokacije artikla nisu mogle biti učitane!", "OK")));
                    });

                    return;
                }

                if (items.Count == 0)
                    ItemTreshold = -1;

                for (int i = 0; i < items.Count; i++)
                {
                    items[i].Location = await Global.GetLocationByAddressAsync(items[i].PharmacyAddressInfo);

                    if (items[i].Location != null)
                    {
                        if (items[i].Location != null)
                            Locations.Add(new Locations
                            {
                                Id = items[i].Location.Id,
                                Address = items[i].Location.Address,
                                Latitude = items[i].Location.Latitude,
                                Longitude = items[i].Location.Longitude,
                                LocationName = items[i].Location.LocationName,
                                Position = new Position(items[i].Location.Latitude, items[i].Location.Longitude)
                            });
                    }
                }

                ItemLocations.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async Task ExecuteSubstituteItemsTresholdReachedCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                List<MedicinDTO> items = await DependencyService.Get<MockItemDataStore>().GetItemsByParamAsync(SubstituteItems.Count.ToString(), Item.Atc);

                if (items == null && items.Count == 0)
                    itemTreshold = -1;

                SubstituteItems.AddRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task SetMapWithLocations()
        {
            try
            {
                if (Locations != null && Locations.Count == 0)
                {
                    for (int i = 0; i < ItemLocations.Count; i++)
                    {
                        ItemLocations[i].Location = await Global.GetLocationByAddressAsync(ItemLocations[i].PharmacyAddressInfo);

                        if (ItemLocations[i].Location != null)
                        {
                            if (ItemLocations[i].Location != null)
                                Locations.Add(new Locations
                                {
                                    Id = ItemLocations[i].Location.Id,
                                    Address = ItemLocations[i].Location.Address,
                                    Latitude = ItemLocations[i].Location.Latitude,
                                    Longitude = ItemLocations[i].Location.Longitude,
                                    LocationName = ItemLocations[i].Location.LocationName,
                                    Position = new Position(ItemLocations[i].Location.Latitude, ItemLocations[i].Location.Longitude)
                                });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
