﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Plugin.Permissions.Abstractions;
using Plugin.Geolocator;
using System.Linq;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
using MvvmHelpers.Commands;
using Rg.Plugins.Popup.Services;
using Apoteka.Views.Popups;

namespace Apoteka.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private UserDTO user;
        public UserDTO User
        {
            get { return user; }
            set { SetProperty<UserDTO>(ref user, value); }
        }

        public static string LoggedUserName
        {
            get => AppSettings.GetValueOrDefault(nameof(LoggedUserName), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(LoggedUserName), value);
        }
        public static string LoggedId
        {
            get => AppSettings.GetValueOrDefault(nameof(LoggedId), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(LoggedId), value);
        }

        private string signInButtonText = "PRIJAVI SE";
        public string SignInButtonText
        {
            get { return signInButtonText; }
            set { SetProperty<string>(ref signInButtonText, value); }
        }

        private static ISettings AppSettings =>
                        CrossSettings.Current;
        
        public ICommand LoginUserCommand { get; set; }

        public LoginViewModel()
        {
            Title = "Login";
            User = new UserDTO();
            LoginUserCommand = new AsyncCommand(ExecuteLoginUserCommand);
        }

        async Task ExecuteLoginUserCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            SignInButtonText = "";

            try
            {
                var user = await DependencyService.Get<MockUserDataStore>().GetItemByEmailAsync(User);

                if (user != null)
                {
                    LoggedUserName = user[0].Username;
                    LoggedId = user[0].Id.ToString();

                    await Application.Current.MainPage.Navigation.PopAsync();
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await PopupNavigation.Instance.PushAsync(new Popup(new PopupViewModel("Info", "Molimo da unesete ispravne podatke.", "OK")));
                    });
                }
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
                SignInButtonText = "PRIJAVI SE";
            }
        }
    }
}
