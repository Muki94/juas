﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.ViewModels
{
    public class PopupViewModel : BaseViewModel
    {
        private string headerText = "";
        public string HeaderText
        {
            get
            {
                return headerText;
            }
            set
            {
                SetProperty(ref headerText, value);
            }
        }

        private string message = "";
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                SetProperty(ref message, value);
            }
        }

        private string buttonText = "";
        public string ButtonText
        {
            get
            {
                return buttonText;
            }
            set
            {
                SetProperty(ref buttonText, value);
            }
        }

        public PopupViewModel()
        {
            HeaderText = "Info";
            Message = "";
            ButtonText = "OK";
        }

        public PopupViewModel(string _headerText, string _message, string _buttonText)
        {
            HeaderText = _headerText;
            Message = _message;
            ButtonText = _buttonText;
        }
    }
}
