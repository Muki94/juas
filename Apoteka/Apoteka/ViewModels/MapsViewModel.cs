﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.HelperModels;
using Apoteka.Helpers;
using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Apoteka.ViewModels
{
    public class MapsViewModel : BaseViewModel
    {
        public ObservableRangeCollection<Locations> Locations { get; set; }

        public ICommand LoadLocationsCommand { get; set; }

        public MapsViewModel()
        {
            Title = "Lokacije Apoteka";
            Locations = new ObservableRangeCollection<Locations>();
            LoadLocationsCommand = new AsyncCommand(ExecuteLoadLocationsCommand);
        }

        public MapsViewModel(LocationDTO location)
        {
            Title = "Lokacije Apoteka";

            Locations = new ObservableRangeCollection<Locations>();
            Locations.Add(new Locations
            {
                Id = location.Id,
                Address = location.Address,
                Latitude = location.Latitude,
                Longitude = location.Longitude,
                LocationName = location.LocationName,
                Position = new Position(location.Latitude, location.Longitude)
            });

            LoadLocationsCommand = new AsyncCommand(ExecuteLoadLocationsCommand);
        }

        public async Task ExecuteLoadLocationsCommand()
        {

            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                if (Locations != null && Locations.Count == 0)
                {
                    var addresses = (await DependencyService.Get<MockPharmacyDataStore>().GetItemsAsync())?.Select(x => x.Address + "," + x.City).ToList();

                    foreach (var address in addresses)
                    {
                        var addressGeolocation = await Global.GetLocationByAddressAsync(address);

                        if (addressGeolocation != null)
                            Locations.Add(new Locations
                            {
                                Id = addressGeolocation.Id,
                                Address = addressGeolocation.Address,
                                Latitude = addressGeolocation.Latitude,
                                Longitude = addressGeolocation.Longitude,
                                LocationName = addressGeolocation.LocationName,
                                Position = new Position(addressGeolocation.Latitude, addressGeolocation.Longitude)
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
