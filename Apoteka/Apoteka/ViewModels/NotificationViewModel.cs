﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Services;
using Apoteka.Interfaces;
using MvvmHelpers.Commands;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Apoteka.ViewModels
{
    public class NotificationViewModel: BaseViewModel
    {
        public INotificationService _notificationService;
        public ICommand ShowUserNotificationsCommand;

        public NotificationViewModel()
        {
            _notificationService = DependencyService.Get<INotificationService>();
            ShowUserNotificationsCommand = new AsyncCommand(ExecuteShowUserNotificationsCommand);
        }

        async Task ExecuteShowUserNotificationsCommand()
        {
            var userId = LoginViewModel.LoggedId;

            if (String.IsNullOrEmpty(userId))
                return;

            var notificationList = await DependencyService.Get<MockNotificationDataStore>().GetItemsByUserIdAsync(userId);

            if (notificationList != null && notificationList.Count > 0)
            {
                foreach (var item in notificationList)
                {
                    var pharmacy = await DependencyService.Get<MockPharmacyDataStore>().GetItemAsync(Convert.ToInt32(item.PharmacyId));

                    if (pharmacy == null)
                        continue;

                    var medicin = await DependencyService.Get<MockPharmacyMedicinDataStore>().GetItemByPharmacyAsync(item.ProductId, item.PharmacyId);

                    if (medicin == null || medicin.Count == 0)
                        continue;

                    _notificationService.ShowNotification("Pristigao Lijek", $"U apoteku {pharmacy.Text} jer pristigao lijek {medicin[0].Name} kojeg ste pratili!");

                    await DependencyService.Get<MockNotificationDataStore>().UpdateItemAsync(new NotificationDTO
                    {
                        PharmacyId = pharmacy.Id.ToString(),
                        ProductId = medicin[0].Id.ToString(),
                        UserId = userId
                    });
                }
            }
        }
    }
}
