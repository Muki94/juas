﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Apoteka.WebApi.Controllers
{
    public class PharmacyController : ApiController
    {
        public HttpResponseMessage GetPharmacies()
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var pharmacies = server.getPharmacies();

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(pharmacies, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetPharacy(string pharmacyId)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var pharmacy = server.getPharmacie(pharmacyId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(pharmacy, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }
    }
}
