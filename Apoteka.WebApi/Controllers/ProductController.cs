﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Apoteka.WebApi.Controllers
{
    public class ProductController : ApiController
    {
        public HttpResponseMessage GetProducts(string searchParameter = "", string itemIndex = "0")
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var products = server.getProducts(itemIndex, searchParameter);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(products, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetProduct(string productId, string itemIndex="0")
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var product = server.getProduct(productId,itemIndex);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(product, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetProductByPharmacyId(string productId, string pharmacyId)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var product = server.getProductByPharmacie(pharmacyId, productId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(product, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetProductsByPharmacyId(string pharmacyId, string searchParameter, string itemIndex = "0")
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var products = server.getProductsByPharmacie(pharmacyId, itemIndex, searchParameter);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(products, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetPictures()
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var pictures = server.getPictures();

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(pictures, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }
    }
}
