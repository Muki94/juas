﻿using Apoteka.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Apoteka.WebApi.Controllers
{
    public class NotificationController : ApiController
    {
        public HttpResponseMessage GetNotificationsByUserId(string userId)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notifications = server.getNotificationsByUser(userId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notifications, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        public HttpResponseMessage GetActiveNotificationsByUserId(string userId)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notifications = server.getActiveNotificationsByUser(userId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notifications, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage InsertNotification([FromBody] Notification notification)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notificationResponse = server.insertNotification(notification.UserId, notification.ProductId, notification.PharmacyId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notificationResponse, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateNotification([FromBody] Notification notification)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notificationResponse = server.updateNotification(notification.UserId, notification.ProductId, notification.PharmacyId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notificationResponse, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateNotificationSend([FromBody] Notification notification)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notificationResponse = server.updateNotificationSend(notification.UserId, notification.ProductId, notification.PharmacyId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notificationResponse, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage DeleteNotification([FromBody] Notification notification)
        {
            try
            {
                WebReference.juasMobileAppServer server = new WebReference.juasMobileAppServer();

                var notificationResponse = server.deleteNotification(notification.UserId, notification.ProductId, notification.PharmacyId);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(notificationResponse, Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError);
                response.Content = new StringContent($"{ex.Message}, {ex.InnerException?.Message}", Encoding.UTF8, "application/json");

                return response;
            }
        }
    }
}
