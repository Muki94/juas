﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apoteka.WebApi.Models
{
    public class Notification
    {
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public string PharmacyId { get; set; }
    }
}