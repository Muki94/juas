﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Apoteka.BLL.DataTransferObjects
{
    public class MedicinDTO
    {
        public int Id { get; set; }
        public string PharmacyId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Type { get; set; }
        public string Atc { get; set; }
        public string Inn { get; set; }
        public string Availibility { get; set; }

        public string IsAvailableText { get; set; }
        public string IsAvailableColor { get; set; }
        public bool CanFollowItem { get; set; }
        public bool IsFollowed { get; set; }
        public bool OldFollowedValue { get; set; }
    }
}