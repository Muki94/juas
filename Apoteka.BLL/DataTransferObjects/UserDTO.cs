﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.DataTransferObjects
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
