﻿namespace Apoteka.BLL.DataTransferObjects
{
    public class LocationDTO
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
        public string LocationName { get; set; }
    }
}
