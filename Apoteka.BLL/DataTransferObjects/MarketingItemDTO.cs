﻿namespace Apoteka.BLL.DataTransferObjects
{
    public class MarketingItemDTO
    {
        public int Id { get; set; }
        public byte[] Image { get; set; } 
    }
}
