﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.DataTransferObjects
{
    public class PharmacyMedicinDTO
    {
        public string PharmacyId { get; set; }
        public string PharmacyAddress { get; set; }
        public string PharmacyCity { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyAddressInfo
        {
            get { return PharmacyCity + "," + PharmacyAddress; }
        }

        public LocationDTO Location { get; set; }
        public string Availability { get; set; }
        public int MedicinId { get; set; }
    }
}
