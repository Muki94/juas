﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.DataTransferObjects
{
    public class PharmacyDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string AddressInfo { get
            {
                return City + "," + Address;
            } 
        }
    }
}
