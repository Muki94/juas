﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.DataTransferObjects
{
    public class ContactDTO
    {
        public string DepartmentName { get; set; }
        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFax { get; set; }
        public string DepartmentEmail { get; set; }
    }
}
