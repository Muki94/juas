﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.DataTransferObjects
{
    public class NotificationDTO
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public string PharmacyId { get; set; }
        public bool Follow { get; set; }
    }
}
