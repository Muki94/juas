﻿namespace Apoteka.BLL.DataTransferObjects
{
    public enum MenuItemType
    {
        Browse,
        About,
        Maps
    }
    public class HomeMenuItemDTO
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
