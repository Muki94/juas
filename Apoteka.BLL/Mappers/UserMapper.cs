﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class UserMapper
    {
        public static UserDTO MapUserToUserDTO(this User model)
        {
            return new UserDTO
            {
                Id = Convert.ToInt32(model.Id),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Username = model.Username,
                Email = model.Email,
                Message = model.Message,
                Code = model.Code
            };
        }

        public static List<UserDTO> MapUserToUserDTO(this List<User> model)
        {
            List<UserDTO> userDTO = new List<UserDTO>();

            model.ForEach((x) => {
                userDTO.Add(x.MapUserToUserDTO());
            });

            return userDTO;
        }
    }
}
