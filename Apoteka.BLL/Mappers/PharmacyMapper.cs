﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class PharmacyMapper
    {
        public static PharmacyDTO MapPharmacyToPharmacyDTO(this Pharmacy model)
        {
            return new PharmacyDTO
            {
                Id = Convert.ToInt32(model.Id),
                Address = model.Address,
                City = model.City,
                Email = model.Email,
                Fax = model.Fax,
                Phone = model.Phone,
                Text = model.Name
            };
        }

        public static List<PharmacyDTO> MapPharmacyToPharmacyDTO(this List<Pharmacy> model)
        {
            List<PharmacyDTO> pharmacyDTO = new List<PharmacyDTO>();

            model.ForEach((x) => {
                pharmacyDTO.Add(x.MapPharmacyToPharmacyDTO());
            });

            return pharmacyDTO;
        }
    }
}
