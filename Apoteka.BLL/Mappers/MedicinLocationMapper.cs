﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class MedicinLocationMapper
    {
        public static PharmacyMedicinDTO MapPharmacyMedicinToPharmacyMedicinDTO(this PharmacyMedicin model)
        {
            return new PharmacyMedicinDTO
            {
                PharmacyId = model.PharmacyId,
                Availability = model.Availability
            };
        }

        public static List<PharmacyMedicinDTO> MapPharmacyMedicinToPharmacyMedicinDTO(this List<PharmacyMedicin> model)
        {
            List<PharmacyMedicinDTO> pharmacyMedicin = new List<PharmacyMedicinDTO>();

            model.ForEach((x) => {
                pharmacyMedicin.Add(x.MapPharmacyMedicinToPharmacyMedicinDTO());
            });

            return pharmacyMedicin;
        }
    }
}
