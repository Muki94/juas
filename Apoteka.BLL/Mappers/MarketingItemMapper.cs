﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class MarketingItemMapper
    {
        public static MarketingItemDTO MapMarketingItemToMarketingItemDTO(this MarketingItem model)
        {
            return new MarketingItemDTO
            {
                Id = Convert.ToInt32(model.Id),
                Image = Convert.FromBase64String(model.Picture),
            };
        }

        public static List<MarketingItemDTO> MapMarketingItemToMarketingItemDTO(this List<MarketingItem> model)
        {
            List<MarketingItemDTO> marketinItemDTO = new List<MarketingItemDTO>();

            model.ForEach((x) => {
                marketinItemDTO.Add(x.MapMarketingItemToMarketingItemDTO());
            });

            return marketinItemDTO;
        }
    }
}
