﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class MedicinMapper
    {
        public static MedicinDTO MapMedicinToMedicinDTO(this Medicin model, bool truncateName = false)
        {
            return new MedicinDTO
            {
                Id = Convert.ToInt32(model.Id),
                Atc = model.Atc,
                Availibility = model.Availibility,
                Code = model.Code,
                Inn = model.Inn,
                Manufacturer = model.Manufacturer,
                Name = truncateName ? (model.Name.Length > 30 ? model.Name.Substring(0, 30) + "..." : model.Name)
                                    : model.Name,
                PharmacyId = model.PharmacyId,
                Type = model.Type
            };
        }

        public static List<MedicinDTO> MapMedicinToMedicinDTO(this List<Medicin> model, bool truncateName = false)
        {
            List<MedicinDTO> medicinDTO = new List<MedicinDTO>();

            model.ForEach((x) => {
                medicinDTO.Add(x.MapMedicinToMedicinDTO(truncateName));
            });

            return medicinDTO;
        }
    }
}
