﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.BLL.Mappers
{
    public static class NotificationMapper
    {
        public static NotificationDTO MapNotificationToNotificationDTO(this Notification model)
        {
            return new NotificationDTO
            {
                UserId = model.UserId,
                ProductId = model.ProductId,
                PharmacyId = model.PharmacyId
            };
        }

        public static List<NotificationDTO> MapNotificationToNotificationDTO(this List<Notification> model)
        {
            List<NotificationDTO> notificationDTO = new List<NotificationDTO>();

            model.ForEach((x) =>
            {
                notificationDTO.Add(x.MapNotificationToNotificationDTO());
            });

            return notificationDTO;
        }

        public static Notification MapNotificationDTOToNotification(this NotificationDTO model)
        {
            return new Notification
            {
                UserId = model.UserId,
                ProductId = model.ProductId,
                PharmacyId = model.PharmacyId
            };
        }

        public static List<Notification> MapNotificationDTOToNotification(this List<NotificationDTO> model)
        {
            List<Notification> notification = new List<Notification>();

            model.ForEach((x) =>
            {
                notification.Add(x.MapNotificationDTOToNotification());
            });

            return notification;
        }
    }
}
