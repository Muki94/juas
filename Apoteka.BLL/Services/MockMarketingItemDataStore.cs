﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.BLL.Mappers;
using Apoteka.DAL.ApiServices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MockMarketingItemDataStore: IDataStore<MarketingItemDTO>
    {
        ProductService productService;

        public MockMarketingItemDataStore()
        {
            productService = new ProductService();
        }

        public Task<bool> AddItemAsync(MarketingItemDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<MarketingItemDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MarketingItemDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            var images = await productService.GetComercialPictures();
            return await Task.FromResult(images?.MapMarketingItemToMarketingItemDTO());
        }

        public Task<List<MarketingItemDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(MarketingItemDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
