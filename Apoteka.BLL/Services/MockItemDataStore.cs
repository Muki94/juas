﻿using Apoteka.BLL.Interfaces;
using Apoteka.BLL.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apoteka.DAL.ApiServices;
using Apoteka.BLL.Mappers;
using System.Collections.ObjectModel;

namespace Apoteka.BLL.Services
{
    public class MockItemDataStore : IDataStore<MedicinDTO>
    {
        //private List<MedicinDTO> items;
        private ProductService productService;
        private PharmacyService pharmacyService;

        public MockItemDataStore()
        {
            productService = new ProductService();
            pharmacyService = new PharmacyService();
        }

        public async Task<List<PharmacyMedicinDTO>> GetItemLocationsAsync(int id, int itemIndex)
        {
            var medicinLocations = (await productService.GetMedicinLocations(id.ToString(), itemIndex.ToString()))?.MapPharmacyMedicinToPharmacyMedicinDTO();

            if (medicinLocations == null)
                return null;

            for (int i = 0; i < medicinLocations.Count; i++)
            {
                var pharmacy = await pharmacyService.GetPharmacy(medicinLocations[i].PharmacyId);

                medicinLocations[i].PharmacyAddress = pharmacy.Address;
                medicinLocations[i].PharmacyCity = pharmacy.City;
                medicinLocations[i].PharmacyName = pharmacy.Name;
            }

            return medicinLocations;
        }

        public async Task<List<MedicinDTO>> GetItemsAsync(string itemIndex, bool forceRefresh = false)
        {
            var medicins = await productService.GetMedicins(itemIndex, "");

            return medicins?.MapMedicinToMedicinDTO(true);
        }

        public async Task<List<MedicinDTO>> GetItemsByParamAsync(string itemIndex, string searchParam, bool forceRefresh = false)
        {
            var medicins = await productService.GetMedicins(itemIndex, searchParam);

            return await Task.FromResult(medicins.MapMedicinToMedicinDTO());
        }

        #region not in use
        public Task<List<MedicinDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<List<MedicinDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MedicinDTO>> GetItemsByIdsAsync(List<int> ids, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> AddItemAsync(MedicinDTO item)
        {
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(MedicinDTO item)
        {
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(int id)
        {
            return await Task.FromResult(true);
        }

        public Task<MedicinDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}