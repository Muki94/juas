﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MockLocationDataStore : IDataStore<LocationDTO>
    {
        List<LocationDTO> locations;

        public MockLocationDataStore()
        {
            locations = new List<LocationDTO>()
            {
                new LocationDTO { Id = 1, Latitude = 43.8536184, Longitude = 18.4056818 , Address = "Maršala Tita 1", LocationName = "Apoteka 1" },
                new LocationDTO { Id = 2, Latitude = 43.8536184, Longitude = 18.4056818 , Address = "Trg Fra Grge Martića 2", LocationName = "Apoteka 2" },
                new LocationDTO { Id = 3, Latitude = 43.8536184, Longitude = 18.4056818 , Address = "Zagrebačka 27", LocationName = "Apoteka 3" },
                new LocationDTO { Id = 4, Latitude = 43.8490381, Longitude = 18.3785593 , Address = "Zmaja od Bosne 51", LocationName = "Apoteka 4" },
                new LocationDTO { Id = 5, Latitude = 43.8479859, Longitude = 18.3754264 , Address = "Gradačačka 14", LocationName = "Apoteka 5" },
                new LocationDTO { Id = 6, Latitude = 43.8684401, Longitude = 18.4077417 , Address = "Hizme Polovine", LocationName = "Apoteka 6" }
            };
        }

        public Task<LocationDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<LocationDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(locations);
        }

        public Task<List<LocationDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddItemAsync(LocationDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(LocationDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
