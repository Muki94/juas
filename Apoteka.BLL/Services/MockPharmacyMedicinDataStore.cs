﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.BLL.Mappers;
using Apoteka.DAL.ApiServices;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MockPharmacyMedicinDataStore : IDataStore<PharmacyMedicinDTO>
    {
        private ProductService productService;

        public MockPharmacyMedicinDataStore()
        {
            productService = new ProductService();
        }

        public async Task<PharmacyMedicinDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PharmacyMedicinDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<List<PharmacyMedicinDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MedicinDTO>> GetItemsByPharmacyAsync(int pharmacyId, string itemIndex, string searchParameter, bool forceRefresh = false)
        {
            if (pharmacyId == default(int))
                throw new NullReferenceException();

            var productsForPharmacy = await productService.GetMedicinsByPharmacyId(pharmacyId.ToString(), itemIndex, searchParameter);

            return productsForPharmacy?.MapMedicinToMedicinDTO();
        }

        public async Task<List<MedicinDTO>> GetItemByPharmacyAsync(string medicinId, string pharmacyId, bool forceRefresh = false)
        {
            if (String.IsNullOrEmpty(pharmacyId) || String.IsNullOrEmpty(medicinId))
                throw new NullReferenceException();

            var productsForPharmacy = await productService.GetMedicinByPharmacyId(medicinId, pharmacyId);

            return productsForPharmacy.MapMedicinToMedicinDTO();
        }


        public async Task<bool> AddItemAsync(PharmacyMedicinDTO item)
        {
            try
            {
                //items.Add(item);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                //TODO
                //Implement error logging
                return await Task.FromResult(false);
            }
        }

        public async Task<bool> UpdateItemAsync(PharmacyMedicinDTO item)
        {
            //var oldItem = items.Where(x => x.Id == item.Id).FirstOrDefault();
            //items.Remove(oldItem);
            //items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(int id)
        {
            //var oldItem = items.Where(x => x.Id == id).FirstOrDefault();
            //items.Remove(oldItem);

            return await Task.FromResult(true);
        }
    }
}
