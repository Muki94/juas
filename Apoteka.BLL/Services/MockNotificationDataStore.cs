﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.BLL.Mappers;
using Apoteka.DAL.ApiServices;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MockNotificationDataStore : IDataStore<NotificationDTO>
    {
        private NotificationService notificationService;

        public MockNotificationDataStore()
        {
            notificationService = new NotificationService();
        }

        public async Task<bool> AddItemAsync(NotificationDTO item)
        {
            try
            {
                return await notificationService.InsertNotification(item.MapNotificationDTOToNotification());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> DeleteNotificationAsync(NotificationDTO item)
        {
            try
            {
                return await notificationService.DeleteNotification(new Notification { 
                    PharmacyId = item.PharmacyId,
                    ProductId = item.ProductId,
                    UserId = item.UserId
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<List<NotificationDTO>> GetItemsByUserIdAsync(string userId, bool forceRefresh = false)
        {
            try
            {
                var notifications = await notificationService.GetNotificationsByUserId(userId);

                return notifications.MapNotificationToNotificationDTO();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<NotificationDTO>> GetActiveItemsByUserIdAsync(string userId, bool forceRefresh = false)
        {
            try
            {
                var notifications = await notificationService.GetActiveNotificationsByUserId(userId);

                return notifications.MapNotificationToNotificationDTO();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> UpdateItemAsync(NotificationDTO item)
        {
            try
            {
                return await notificationService.UpdateNotificationSend(item.MapNotificationDTOToNotification());
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region not in use
        public Task<bool> DeleteItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<NotificationDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<NotificationDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<List<NotificationDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
