﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.BLL.Mappers;
using Apoteka.DAL.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MockPharmacyDataStore : IDataStore<PharmacyDTO>
    {
        private PharmacyService pharmacyService;

        public MockPharmacyDataStore()
        {
            pharmacyService = new PharmacyService();
        }

        public Task<bool> AddItemAsync(PharmacyDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<PharmacyDTO> GetItemAsync(int id)
        {
            var pharmacy = await pharmacyService.GetPharmacy(id.ToString());

            return pharmacy.MapPharmacyToPharmacyDTO();
        }

        public async Task<List<PharmacyDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            var pharmacies = await pharmacyService.GetPharmacies();

            return pharmacies?.MapPharmacyToPharmacyDTO();
        }

        public async Task<List<PharmacyDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            var pharmacies = await pharmacyService.GetPharmacies();

            var tempItems = pharmacies.Where(x => x.City.ToUpper().Contains(searchParam.ToUpper()) ||
                                                  x.Address.ToUpper().Contains(searchParam.ToUpper()))
                                      .ToList()
                                      .MapPharmacyToPharmacyDTO();

            return await Task.FromResult(tempItems);
        }

        public Task<bool> UpdateItemAsync(PharmacyDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
