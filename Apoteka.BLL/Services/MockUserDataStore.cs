﻿using Apoteka.BLL.DataTransferObjects;
using Apoteka.BLL.Interfaces;
using Apoteka.BLL.Mappers;
using Apoteka.DAL.ApiServices;
using Apoteka.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
 public   class MockUserDataStore : IDataStore<UserDTO>
    {

        private UserService userService;

        public MockUserDataStore()
        {
            userService = new UserService();
        }
        public async Task<UserDTO> AddItemAsync(UserDTO item)
        {

            User user = await userService.AddNewUser(new User
            {
                //premapirati objekat item u user iz dal sloja
                FirstName = item.FirstName,
                LastName = item.LastName,
                Email = item.Email,
                Username = item.Username
            }) ;

            return user.MapUserToUserDTO();

        }

        public Task<bool> DeleteItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<UserDTO>> GetItemByEmailAsync(UserDTO item)
        {
            List<User> user = await userService.GetCustomerByEmail(new User
            {
                //premapirati objekat item u user iz dal sloja
                FirstName = item.FirstName,
                LastName = item.LastName,
                Email = item.Email,
                Username = item.Username
            });

            if (user == null || user.Count == 0)
                return null;

            return item.Username.Equals(user[0]?.Username) ? user.MapUserToUserDTO() 
                                                           : null;

        }

        public Task<List<UserDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<List<UserDTO>> GetItemsByParamAsync(string searchParam, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(UserDTO item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<UserDTO>.AddItemAsync(UserDTO item)
        {
            throw new NotImplementedException();
        }
        public Task<UserDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

    }
}
