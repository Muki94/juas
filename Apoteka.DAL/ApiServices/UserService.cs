﻿using Apoteka.DAL.Helpers;
using Apoteka.DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.DAL.ApiServices
{
    public class UserService
    {
        public async Task<List<User>> GetCustomerByEmail(User user)
        {
            try
            {
                //var handler = new HttpClientHandler();
                //handler.ServerCertificateCustomValidationCallback =
                //    (message, certificate, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(/*handler*/))
                {
                    client.BaseAddress = new Uri(Constants.UserWebApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArray = Encoding.ASCII.GetBytes(Constants.CustomerKey_CustomerSecret);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var response = await client.GetAsync($"{Constants.GetCustomerByEmail}?email={user.Email}");

                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<User>>(content);
                    }
                   
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<List<User>> GetCustomers()
        {
            try
            {
                //var handler = new HttpClientHandler();
                //handler.ServerCertificateCustomValidationCallback =
                //    (message, certificate, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(/*handler*/))
                {
                    client.BaseAddress = new Uri(Constants.UserWebApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArray = Encoding.ASCII.GetBytes(Constants.CustomerKey_CustomerSecret);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var response = await client.GetAsync(Constants.GetCustomers);

                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<User>>(content);
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<User> GetCustomer(int customerId)
        {
            try
            {
                //var handler = new HttpClientHandler();
                //handler.ServerCertificateCustomValidationCallback =
                //    (message, certificate, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(/*handler*/))
                {
                    client.BaseAddress = new Uri(Constants.UserWebApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArray = Encoding.ASCII.GetBytes(Constants.CustomerKey_CustomerSecret);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var response = await client.GetAsync($"{Constants.GetCustomer}{customerId}");

                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<User>(content);
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<User> AddNewUser(User user)
        {
            try
            {
                //var handler = new HttpClientHandler();
                //handler.ServerCertificateCustomValidationCallback =
                //    (message, certificate, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(/*handler*/))
                {
                    client.BaseAddress = new Uri(Constants.UserWebApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var byteArray = Encoding.ASCII.GetBytes(Constants.CustomerKey_CustomerSecret);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var json = JsonConvert.SerializeObject(new
                    {
                        first_name = user.FirstName,
                        last_name = user.LastName,
                        email = user.Email,
                        username = user.Username
                    });

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Constants.AddNewCustomer);
                    request.Content = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = await client.SendAsync(request);

                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<User>(content);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
