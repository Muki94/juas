﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.DAL.Helpers
{
    public class Constants
    {
        public static string WebApiBaseUrl => "http://juas.logosoft.ba:5876/";
        //public static string WebApiBaseUrl => "https://192.168.65.25:45457/";
        public static string UserWebApiBaseUrl => "https://shop.apoteke-sarajevo.ba/wp-json/wc/v3/";
        public static string CustomerKey_CustomerSecret => "ck_eee24e5ce2d876f548fa3f2f829b2bc2331d2f36:cs_72d010989efe3c5ae394a6aa6b08e63ff6ed3e6f";

        //Web API urls
        public static string GetProducts => "api/Product/GetProducts";
        public static string GetProduct => "api/Product/GetProduct";
        public static string GetProductByPharmacyId => "api/Product/GetProductByPharmacyId";
        public static string GetProductsByPharmacyId => "api/Product/GetProductsByPharmacyId";
        public static string GetPictures => "api/Product/GetPictures";

        public static string GetPharmacies => "api/Pharmacy/GetPharmacies";
        public static string GetPharmacy => "api/Pharmacy/GetPharacy";

        public static string GetNotificationsByUserId => "api/Notification/GetNotificationsByUserId";
        public static string GetActiveNotificationsByUserId => "api/Notification/GetActiveNotificationsByUserId";
        public static string InsertNotification => "api/Notification/InsertNotification";
        public static string UpdateNotificationSend => "api/Notification/UpdateNotificationSend";
        public static string DeleteNotification => "api/Notification/DeleteNotification";

        public static string GetCustomerByEmail => "customers";
        public static string GetCustomers => "customers";
        public static string GetCustomer => "customers/";
        public static string AddNewCustomer => "customers";
    }
}
