﻿using Newtonsoft.Json;

namespace Apoteka.DAL.Models
{
    public class Location
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Address { get; set; }
        public string LocationName { get; set; }
    }
}
