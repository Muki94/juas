﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.DAL.Models
{
    public class Notification
    {
        public string Id { get; set; }

        [JsonProperty("id_korisnika")]
        public string UserId { get; set; }

        [JsonProperty("id_proizvoda")]
        public string ProductId { get; set; }

        [JsonProperty("id_apoteke")]
        public string PharmacyId { get; set; }
    }
}
