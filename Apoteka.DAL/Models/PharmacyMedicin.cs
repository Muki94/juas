﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.DAL.Models
{
    public class PharmacyMedicin
    {
        public int Id { get; set; }
        public int MedicinId { get; set; }
        public int Quantity { get; set; }

        [JsonProperty("pj")]
        public string PharmacyId { get; set; }

        [JsonProperty("dostupnost")]
        public string Availability { get; set; }
    }
}
