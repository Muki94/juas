﻿using Newtonsoft.Json;

namespace Apoteka.DAL.Models
{
    public class MarketingItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("picture")]
        public string Picture { get; set; }
    }
}
