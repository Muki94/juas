﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Apoteka.DAL.Models
{
    public class Medicin
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("pj")]
        public string PharmacyId { get; set; }

        [JsonProperty("sifra")]
        public string Code { get; set; }

        [JsonProperty("naziv")]
        public string Name { get; set; }

        [JsonProperty("proizvodjac")]
        public string Manufacturer { get; set; }

        [JsonProperty("vrsta")]
        public string Type { get; set; }

        [JsonProperty("atc")]
        public string Atc { get; set; }

        [JsonProperty("inn")]
        public string Inn { get; set; }

        [JsonProperty("dostupnost")]
        public string Availibility { get; set; }
    }
}