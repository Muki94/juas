﻿namespace Apoteka.DAL.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        Maps
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
