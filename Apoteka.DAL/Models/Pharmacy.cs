﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apoteka.DAL.Models
{
    public class Pharmacy
    {
        [JsonProperty("sifra")]
        public string Id { get; set; }

        [JsonProperty("naziv")]
        public string Name { get; set; }

        [JsonProperty("adresa")]
        public string Address { get; set; }

        [JsonProperty("grad")]
        public string City { get; set; }

        [JsonProperty("pib")]
        public string Pib { get; set; }

        [JsonProperty("jib")]
        public string Jib { get; set; }

        [JsonProperty("telefon")]
        public string Phone { get; set; }

        [JsonProperty("fax")]
        public string Fax { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("kanton")]
        public string Region { get; set; }

        [JsonProperty("dezurna")]
        public string OnDuty { get; set; }

    }
}
